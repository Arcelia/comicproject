<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class ComicsController extends Controller{

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index($consult){
        switch ($consult) {
            case 'L1':
                return \DB::table('comics')->orderBy('id', 'desc')->limit(12)->get();
                break;
            case 'L2':
                return \DB::table('comicLists')
                ->select(\DB::raw('count(comicLists.comics_id) as count_id'),
                    'comics.id', 'comics.name', 'comics.cover', 'comics.description', 'comics.type_id','comics.user_id', 
                    'comics.statusComic_id')
                ->join('comics','comicLists.comics_id', '=', 'comics.id')
                ->where('comicLists.statusList_id', 2)
                ->groupBy('comicLists.comics_id')
                ->orderBy('count_id')
                ->limit(12)
                ->get();
                break;
             case 'L3':
                return \DB::table('comics')
                ->select('comics.id', 'comics.name as comicName', 'comics.description', 'comics.cover', 'comics.user_id',
                    'comics.type_id', 'types.name as typeName',
                    'comics.statusComic_id', 'statusComics.name as statusComicsName',
                    'comics.classification_id', 'classifications.name as classificationsName')
                ->join('types','types.id', '=', 'comics.type_id')
                ->join('statusComics','comics.statusComic_id', '=', 'statusComics.id')
                ->join('classifications','comics.classification_id', '=', 'classifications.id')
                ->get();
                break;
            default:
                return \DB::table('comics')->get();
                break;
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        //
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //
    }
}