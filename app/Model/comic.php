<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class comic extends Model{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array[
        'name', 'description', 'user_id', 'statusComic_id', 'types_id'
    ];

    /**
     * The relationships.
     *
     * @return
     */
    //-chapter
    //-comicList
    //-genderComic
    //user_id
    //statusComic_id
    //types_id
}