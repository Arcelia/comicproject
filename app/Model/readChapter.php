    <?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class readChapter extends Model{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'chapter_id'
    ];

    /**
     * The relationships.
     *
     * @return
     */
    //user_id
    //chapter_id
}