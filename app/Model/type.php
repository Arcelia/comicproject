    <?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class type extends Model{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * The relationships.
     *
     * @return
     */
    //-comic
}