<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class page extends Model{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number', 'pathImage', 'chapter_id'
    ];

    /**
     * The relationships.
     *
     * @return
     */
    //chapter_id
}