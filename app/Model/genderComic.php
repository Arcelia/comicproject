<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class genderComic extends Model{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gender_id', 'comic_id'
    ];

    /**
     * The relationships.
     *
     * @return
     */
    //gender_id
    //comic_id
}