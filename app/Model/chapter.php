<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class chapter extends Model{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array[
        'number', 'name', 'pathCover'
    ];

    /**
     * The relationships.
     *
     * @return
     */
    //comic_id
    //-page
    //-readChapter
}