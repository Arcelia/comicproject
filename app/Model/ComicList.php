<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class ComicList extends Model{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'user_id', 'comic_id', 'statusList_id'
    ];

    /**
     * The relationships.
     *
     * @return
     */
    //user_id
    //comic_id
    //statusList_id
}