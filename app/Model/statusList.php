<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class statusList extends Model{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name', 'description'
    ];

    /**
     * The relationships.
     *
     * @return
     */
    //-comicList
}