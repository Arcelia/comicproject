<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Password Token
 * @author Arcelia Aguirre
 * @since 08/10/2017
*/
class CreatePasswordResetsTable extends Migration{
    /**
     * Run the migrations.
     * @return void
     */
    public function up(){
        Schema::create('passwordResets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at');
        });
    }
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down(){
        Schema::dropIfExists('passwordResets');
    }
}