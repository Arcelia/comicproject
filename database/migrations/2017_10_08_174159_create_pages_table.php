<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Chapters pages
 * @author Arcelia Aguirre
 * @since 08/10/2017
*/
class CreatePagesTable extends Migration{
    /**
     * Run the migrations.
     * @return void
     */
    public function up(){
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number')->notnullable();
            $table->string('pathImage')->notnullable();
            $table->integer('chapter_id')->unsigned();
            $table->foreign('chapter_id')->references('id')->on('chapters');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down(){ 
        Schema::dropIfExists('pages');
    }
}