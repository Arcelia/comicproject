<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Chapters
 * @author Arcelia Aguirre
 * @since 08/10/2017
*/
class CreateChaptersTable extends Migration{
    /**
     * Run the migrations.
     * @return void
     */
    public function up(){
        Schema::create('chapters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number')->notnullable();
            $table->string('name');
            $table->string('pathCover');
            $table->integer('comic_id')->unsigned();
            $table->foreign('comic_id')->references('id')->on('comics');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down(){ 
        Schema::dropIfExists('chapters');
    }
}