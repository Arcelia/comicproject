<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Comic
 * @author Arcelia Aguirre
 * @since 08/10/2017
*/
class CreateComicsTable extends Migration{
    /**
     * Run the migrations.
     * @return void
     */
    public function up(){
        Schema::create('comics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->notnullable();
            $table->string('description');
            $table->string('cover');
            $table->integer('user_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->integer('statusComic_id')->unsigned();
            $table->integer('classification_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('statusComic_id')->references('id')->on('statusComics');
            $table->foreign('classification_id')->references('id')->on('classifications');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down(){
        Schema::dropIfExists('comics');
    }
}