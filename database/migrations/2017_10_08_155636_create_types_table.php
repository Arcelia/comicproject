<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Character type
 * @author Arcelia Aguirre
 * @since 08/10/2017
*/
class CreateTypesTable extends Migration{
    /**
     * Run the migrations.
     * @return void
     */
    public function up(){
        Schema::create('types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->notnullable();
            $table->string('description');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down(){
        Schema::dropIfExists('types');
    }
}