<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Comic genders
 * @author Arcelia Aguirre
 * @since 08/10/2017
*/
class CreateGendersTable extends Migration{
    /**
     * Run the migrations.
     * @return void
     */
    public function up(){
        Schema::create('genders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->notnullable(); 
            $table->string('description'); 
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down(){
        Schema::dropIfExists('genders');
    }
}