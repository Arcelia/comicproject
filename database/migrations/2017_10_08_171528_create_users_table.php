<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Users
 * @author Arcelia Aguirre
 * @since 08/10/2017
*/
class CreateUsersTable extends Migration{
    /**
     * Run the migrations.
     * @return void
     */
    public function up(){
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->notnullable();
            $table->string('email')->unique();
            $table->string('password')->notnullable();
            $table->date('birth');
            $table->boolean('gender');
            $table->string('pathAvatar');
            $table->string('pathBanner');
            $table->rememberToken();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down(){
        Schema::dropIfExists('users');
    }
}