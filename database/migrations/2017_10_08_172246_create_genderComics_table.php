<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Comic Genders
 * @author Arcelia Aguirre
 * @since 08/10/2017
*/
class CreateGenderComicsTable extends Migration{
    /**
     * Run the migrations.
     * @return void
     */
    public function up(){
        Schema::create('genderComics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gender_id')->unsigned();
            $table->integer('comic_id')->unsigned();
            $table->foreign('gender_id')->references('id')->on('genders');
            $table->foreign('comic_id')->references('id')->on('comics');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down(){ 
        Schema::dropIfExists('genderComicomics');
    }
}