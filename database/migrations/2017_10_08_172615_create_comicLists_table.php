<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * Comic List
 * @author Arcelia Aguirre
 * @since 08/10/2017
*/
class CreateComicListsTable extends Migration{
    /**
     * Run the migrations.
     * @return void
     */
    public function up(){
        Schema::create('comicLists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('comics_id')->unsigned();
            $table->integer('statusList_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('comics_id')->references('id')->on('comics');
            $table->foreign('statusList_id')->references('id')->on('statusLists');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down(){
        Schema::dropIfExists('comicLists');
    }
}