<?php
use Illuminate\Database\Seeder;
class ReadChaptersTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('readChapters')->insert([
            ['user_id' => 1, 'chapter_id' => 1]
        ]);
    }
}