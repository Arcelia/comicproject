<?php
use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run(){
        $this->call(ClassificationsTableSeeder::class);
        $this->call(GendersTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(StatusComicsSeeder::class);
        $this->call(StatusListsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ComicsTableSeeder::class);
        $this->call(ComicListsTableSeeder::class);
    }
}