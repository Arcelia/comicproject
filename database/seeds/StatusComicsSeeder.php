<?php
use Illuminate\Database\Seeder;
class StatusComicsSeeder extends Seeder{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run(){
        DB::table('statusComics')->insert([
            ['name' => 'Emisión', 'description' => 'Se actualiza constantemente'],
            ['name' => 'Pausa', 'description' => 'Por el momento está detenido'],
            ['name' => 'Terminado', 'description' => 'Se finalizó'],
            ['name' => 'Cancelado', 'description' => 'No se va  a acontinuar']
        ]);
    }
}