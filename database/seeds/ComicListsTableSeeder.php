<?php
use Illuminate\Database\Seeder;
class ComicListsTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('comicLists')->insert([
            ['user_id' => 1, 'comics_id' => 1, 'statusList_id' => 1],
            ['user_id' => 1, 'comics_id' => 2, 'statusList_id' => 2],
            ['user_id' => 1, 'comics_id' => 3, 'statusList_id' => 3],
            ['user_id' => 1, 'comics_id' => 4, 'statusList_id' => 4],
            ['user_id' => 1, 'comics_id' => 5, 'statusList_id' => 5],
            ['user_id' => 1, 'comics_id' => 6, 'statusList_id' => 1],
            ['user_id' => 1, 'comics_id' => 7, 'statusList_id' => 2],
            ['user_id' => 1, 'comics_id' => 8, 'statusList_id' => 3],
            ['user_id' => 1, 'comics_id' => 9, 'statusList_id' => 4],
            ['user_id' => 1, 'comics_id' => 10, 'statusList_id' => 5],
            ['user_id' => 2, 'comics_id' => 1, 'statusList_id' => 2],
            ['user_id' => 2, 'comics_id' => 2, 'statusList_id' => 2],
            ['user_id' => 2, 'comics_id' => 3, 'statusList_id' => 3],
            ['user_id' => 2, 'comics_id' => 4, 'statusList_id' => 3],
            ['user_id' => 2, 'comics_id' => 5, 'statusList_id' => 3],
            ['user_id' => 2, 'comics_id' => 6, 'statusList_id' => 2],
            ['user_id' => 2, 'comics_id' => 7, 'statusList_id' => 2],
            ['user_id' => 2, 'comics_id' => 8, 'statusList_id' => 3],
            ['user_id' => 2, 'comics_id' => 9, 'statusList_id' => 4],
            ['user_id' => 2, 'comics_id' => 10, 'statusList_id' => 5],
            ['user_id' => 3, 'comics_id' => 1, 'statusList_id' => 2],
            ['user_id' => 3, 'comics_id' => 2, 'statusList_id' => 2],
            ['user_id' => 3, 'comics_id' => 3, 'statusList_id' => 3],
            ['user_id' => 3, 'comics_id' => 4, 'statusList_id' => 3],
        ]);
    }
}