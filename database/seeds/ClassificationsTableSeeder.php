<?php
use Illuminate\Database\Seeder;
class ClassificationsTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('classifications')->insert([
            ['name' => 'All Ages', 'description' => 'Todas las edades'],
            ['name' => 'T+ ', 'description' => 'Mayores de 13 años'],
            ['name' => 'Parental Advisory', 'description' => 'Mayores de 15 años'],
            ['name' => 'MAX', 'description' => 'Mayores de 18 años'],
            ['name' => 'AA', 'description' => 'Programación apropiada para todo público, especialmente dirigido a público infantil, cualquier horario'],
            ['name' => 'A', 'description' => 'Programación apropiada para todo público, cualquier horario. Puede incluir violencia de fantasía leve o cómica.'],
            ['name' => 'B', 'description' => 'Programación no recomendada para niños menores de 12 años; desde las 16 horas. Puede incluir violencia moderada, lenguaje vulgar ocasional, situaciones sexuales leves, dialogos sugestivos o presencia y consumo de sustancias tóxicas.'],
            ['name' => 'B-15', 'description' => '15 años en adelante y adultos; de las 19 a las 5:59 horas'],
            ['name' => 'C', 'description' => 'Para adultos, de las 21 a las 5:59 horas'],
            ['name' => 'D', 'description' => 'Exclusivamente solo adultos, de las 0 a las 5 horas.']
        ]);
    }
}