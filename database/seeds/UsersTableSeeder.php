<?php
use Illuminate\Database\Seeder;
use Carbon\Carbon;
class UsersTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
         DB::table('users')->insert([
            ['name' => str_random(10), 
            	'email' => str_random(12).'@mail.com',
            	'password' => bcrypt('12345'), 
            	'birth' => Carbon::create('2000', '01', '01'), 
            	'gender' => true,
            	'pathAvatar' => '',
            	'pathBanner' => ''],
            ['name' => str_random(10), 
            	'email' => str_random(12).'@mail.com',
            	'password' => bcrypt('12345'), 
            	'birth' => Carbon::create('2000', '01', '01'), 
            	'gender' => true,
            	'pathAvatar' => '',
            	'pathBanner' => ''],
            ['name' => str_random(10), 
            	'email' => str_random(12).'@mail.com',
            	'password' => bcrypt('12345'), 
            	'birth' => Carbon::create('2000', '01', '01'), 
            	'gender' => true,
            	'pathAvatar' => '',
            	'pathBanner' => ''],
            ['name' => str_random(10), 
            	'email' => str_random(12).'@mail.com',
            	'password' => bcrypt('12345'), 
            	'birth' => Carbon::create('2000', '01', '01'), 
            	'gender' => false,
            	'pathAvatar' => '',
            	'pathBanner' => ''],
            ['name' => str_random(10), 
            	'email' => str_random(12).'@mail.com',
            	'password' => bcrypt('12345'), 
            	'birth' => Carbon::create('2000', '01', '01'), 
            	'gender' => false,
            	'pathAvatar' => '',
            	'pathBanner' => ''],
            ['name' => str_random(10), 
            	'email' => str_random(12).'@mail.com',
            	'password' => bcrypt('12345'), 
            	'birth' => Carbon::create('2000', '01', '01'), 
            	'gender' => false,
            	'pathAvatar' => '',
            	'pathBanner' => ''],
        ]);
    }
}