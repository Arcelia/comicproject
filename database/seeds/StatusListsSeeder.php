<?php
use Illuminate\Database\Seeder;
class StatusListsSeeder extends Seeder{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run(){
        DB::table('statusLists')->insert([
            ['name' => 'Quiero leer', 'description' => 'Lista de deseos'],
            ['name' => 'Leyendo', 'description' => 'Siguiendo'],
            ['name' => 'Terminado', 'description' => 'Lo lei'],
            ['name' => 'No leer', 'description' => 'No me interesa'],
            ['name' => 'Dejar de leer', 'description' => 'No me interesó'],
        ]);
    }
}