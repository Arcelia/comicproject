<?php
use Illuminate\Database\Seeder;
class GendersTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */	
    public function run(){
        DB::table('genders')->insert([
            ['name' => 'Ciencia Ficción', 'description' => ''],
            ['name' => 'Comedia', 'description' => ''],
            ['name' => 'Superheroes', 'description' => ''],
            ['name' => 'Infantil', 'description' => ''],
            ['name' => 'Fantasía', 'description' => ''],
            ['name' => 'Sobrenatural', 'description' => ''],
            ['name' => 'Paranormal', 'description' => ''],
            ['name' => 'Detectives', 'description' => ''],
            ['name' => 'Bélico', 'description' => ''],
            ['name' => 'Histórico', 'description' => ''],
            ['name' => 'Bigrafíco', 'description' => ''],
            ['name' => 'Adulto', 'description' => ''],
            ['name' => 'Sátira', 'description' => ''],
            ['name' => 'Mecha', 'description' => ''],
            ['name' => 'Costumbrista', 'description' => ''],
            ['name' => 'Deportes', 'description' => ''],
            ['name' => 'Policíaco', 'description' => ''],
            ['name' => 'Terror', 'description' => ''],
            ['name' => 'Escolar', 'description' => ''],
            ['name' => 'Romance', 'description' => '']
        ]);
    }
}