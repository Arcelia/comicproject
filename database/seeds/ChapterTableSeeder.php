<?php
use Illuminate\Database\Seeder
class ChapterTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
       DB::table('chapters')->insert([
            ['number' => 1, 'name' => 'Flash Rebirth ', 'pathCover' => '/img/flash/cover',    'comic_id' => 19],
            ['number' => 2, 'name' => 'Flash Rebirth ', 'pathCover' => '/img/flash/cover_02', 'comic_id' => 19],
            ['number' => 3, 'name' => 'Flash Rebirth ', 'pathCover' => '/img/flash/cover_03', 'comic_id' => 19],
            ['number' => 4, 'name' => 'Flash Rebirth ', 'pathCover' => '/img/flash/cover_04', 'comic_id' => 19],
            ['number' => 5, 'name' => 'Flash Rebirth ', 'pathCover' => '/img/flash/cover_05', 'comic_id' => 19]
        ]);
    }
}