<?php
use Illuminate\Database\Seeder;
class PagesTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('pages')->insert([
            ['number' => 1, 'chapter_id' => 1, 'pathImage' => '/img/flash/cover', 'comic_id' => 19],
            ['number' => 2, 'chapter_id' => 1, 'pathImage' => '/img/flash/cover_02', 'comic_id' => 19],
            ['number' => 3, 'chapter_id' => 1, 'pathImage' => '/img/flash/cover_03', 'comic_id' => 19],
            ['number' => 4, 'chapter_id' => 1, 'pathImage' => '/img/flash/cover_04', 'comic_id' => 19],
            ['number' => 5, 'chapter_id' => 1, 'pathImage' => '/img/flash/cover_05', 'comic_id' => 19]
        ]);
    }
}