<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('types')->insert([
            ['name' => 'Boceto previo', 'description' => 'Previo del comic final'],
            ['name' => 'Capítulo', 'description' => 'Capitulo normal'],
            ['name' => 'Especial', 'description' => 'Capítulo doble, final. Diferente al Capítulo normal'],
            ['name' => 'Extra', 'description' => 'Capítulo que no es parte de la trama'],
            ['name' => 'One Shot', 'description' => 'Un solo Capítulo'],
            ['name' => 'Crossover', 'description' => 'Aparición de personajes o lugares de otro comic']
        ]);
    }
}
