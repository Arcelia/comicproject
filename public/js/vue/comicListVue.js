Vue.component('card', {
	template: 
    '<div class="row" :idcomic="idcomic">' +
  		'<div class="col-md-2">' +
        '<a href="#">' +
          '<img class="img-fluid rounded mb-3 mb-md-0" :src="pathcover" alt="">' +
        '</a>' +
      '</div>' +
      '<div class="col-md-10">' +
        '<h4 class="title-font">{{ title }}</h4>' +
        '<p class="text-font">{{ description }}</p>' +
        '<label class="text-font"><strong>Generos</strong></label><br>' +
        '<label class="text-font"><strong>Clasificacion</strong> {{ classificationsname }}</label><br>' +
        '<label class="text-font"><strong>Tipo</strong> {{ typename }} </label><br>' +
        '<label class="text-font"><strong>Historias</strong>  {{ statuscomicname }}</label><br>' +
        '<div class="talk-bubble tri-right btm-right">' +
          '<div class="talktext">' +
          '<a class="title-font" href="comic">Leer comic</a>' +
          '</div>' +
        '</div>' +
      '</div>'+
    '</div>',
  props :['idcomic', 'pathcover', 'title', 'description',
    'statuscomic', 'statuscomicname',
    'type', 'typename',
    'classifications', 'classificationsname']
})

var vm = new Vue({
  el: '#comicList',
  data: {
	}
})