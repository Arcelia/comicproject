Vue.component('card', {
	template: 
		'<div class="col-lg-2 col-md-4 col-sm-6 portfolio-item" :idcomic="idcomic">' +
          '<div class="card h-100">' +
            '<a href="#"><img class="card-img-top" :src="pathcover" alt=""></a>'+
            '<div class="card-body">'+
              '<h6 class="card-title text-center">'+
                '<a class="text-font" href="#">{{ title }}</a>'+
              '</h6>'+
            '</div>'+
          '</div>' +
        '</div>',
  props :['idcomic', 'title', 'pathcover']
})

var vm = new Vue({
  el: '#dashboard',
  data: {
	}

})
