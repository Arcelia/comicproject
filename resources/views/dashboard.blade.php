<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Comic Project</title>
    <!-- Bootstrap core CSS -->
    <link href="js/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="css/reset.css">
    <link href="css/4-col-portfolio.css" rel="stylesheet">
    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link href="css/colors.css" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="css/checkbox.css">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/componentReset.css">
    <link rel="stylesheet" type="text/css" href="css/scrollBar.css">
    <script type="text/javascript" src="js/vue.js"></script>
    
  </head>

  <body>
    <!-- Navigation -->
    @include('section.header')

    <!-- Page Content -->
    <div id="wrapper" class="container">
      <div>
        <div id="sidebar-wrapper">
          <ul class="sidebar-nav">
            <li class="sidebar-brand">
              <label class="white-text title-font">Géneros</label>
            </li>
            @foreach($genders as $gender)
            <li>
              <div class="check white-text">
                <input id="{{ 'gender'.$gender->id }}" type="checkbox"/>
                <label for="{{ 'gender'.$gender->id }}">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">{{ $gender->name }}</p>
            </li>
            @endforeach
            <li class="sidebar-brand">
              <label class="white-text reset-p title-font">Clasificaciones</label>
            </li>
            @foreach($classifications as $classification)
            <li>
              <div class="check white-text">
                <input id="{{ 'classification'.$classification->id }}" type="checkbox"/>
                <label for="{{ 'classification'.$classification->id }}">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">{{ $classification->name }}</p>
            </li>
            @endforeach
            <li class="sidebar-brand">
              <label class="white-text title-font">Tipos</label>
            </li>
            @foreach($types as $type)
            <li>
              <div class="check white-text">
                <input id="{{ 'type'.$type->id }}" type="checkbox"/>
                <label for="{{ 'type'.$type->id }}">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">{{ $type->name }}</p>
            </li>
            @endforeach
            <li class="sidebar-brand">
              <label class="white-text title-font">Historias</label>
            </li>
            @foreach($statusComics as $statusComic)
            <li>
              <div class="check white-text">
                <input id="{{ 'statusComic'.$statusComic->id }}" type="checkbox"/>
                <label for="{{ 'statusComic'.$statusComic->id }}">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">{{ $statusComic->name }}</p>
            </li>
            @endforeach
            <li class="sidebar-brand">
              <label class="white-text title-font">Mis listas</label>
            </li>
            @foreach($statusLists as $statusList)
            <li>
              <div class="check white-text">
                <input id="{{ 'statusList'.$statusList->id }}" type="checkbox"/>
                <label for="{{ 'statusList'.$statusList->id }}">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">{{ $statusList->name }}</p>
            </li>
            @endforeach
          </ul>
        </div>
      </div>
      <a href="#menu-toggle" class="btn btn-secondary text-font" id="menu-toggle">Categorías</a>
      <!-- Page Heading -->
      <h1 class="my-4 title-font">Nuevos
        <small></small>
      </h1>
      <div  id="dashboard">
      <div class="row">
      <!-- 12 cards for section -->
       @foreach($comicsNew as $comicNew)
        <card idcomic='{{ $comicNew->id }}' title='{{ $comicNew->name }}' pathcover='{{ $comicNew->cover }}'></card>
       @endforeach
        
      </div>
      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only text-font">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only text-font">Next</span>
          </a>
        </li>
      </ul>
      <!-- /.row -->
      <h1 class="my-4 title-font">Más vistos
        <small></small>
      </h1>
      <div class="row">
      <!-- 12 cards for section -->
       @foreach($comicsView as $comicView)
        <card idcomic='{{ $comicView->id }}' title='{{ $comicView->name }}' pathcover='{{ $comicView->cover }}'></card>
       @endforeach
        
      </div>
      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only text-font">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only text-font">Next</span>
          </a>
        </li>
      </ul>
      </div>
    </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white text-font">Copyright &copy; ComicProject 2017</p>
        <p class="m-0 text-center text-white text-font">Checkbox Made by Branko Stancevic</p>
        <div cñ>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons" class="text-font">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
      </div>
      <!-- /.container -->
    </footer>
    <script src="js/vendor/jquery/jquery.min.js"></script>
    <script>
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
    </script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/vendor/popper/popper.min.js"></script>
    <script src="js/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/vue/dashboardVue.js"></script>
  </body>
</html>