<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Comic Project</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap-3.3.7/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-3.3.7/dist/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/fontAwesome.css">
        <link rel="stylesheet" href="css/templatemo-style.css">

        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <!-- Custom Fonts -->
        <link rel="stylesheet" type="text/css" href="css/fonts.css">
        <link rel="stylesheet" type="text/css" href="css/componentReset.css">

    </head>
    <body>

        <div class="overlay"></div>
        <section class="top-part">
         	<img class="background-image" src="img/backgraund/landingPage.jpg">
        </section>
        <section class="cd-hero">
          <div class="cd-slider-nav">
            <nav>
              <span class="cd-marker item-1"></span>
              <ul>
                <li class="selected"><a href="#0"><div class="image-icon"><img src="img/icons/icons-32/022-web.png"></div><h6 class="title-font">Inicio</h6></a></li>
                <li><a href="#0"><div class="image-icon"><img src="img/icons/icons-32/009-picture.png"></div><h6 class="title-font">Nosotros</h6></a></li>
                <li><a href="#0"><div class="image-icon"><img src="img/icons/icons-32/008-book.png"></div><h6 class="title-font">Comics</h6></a></li>
                <li><a href="#0"><div class="image-icon"><img src="img/icons/icons-32/020-back.png"></div><h6 class="title-font">Contaco</h6></a></li>
                <li><a href="#0"><div class="image-icon"><img src="img/icons/icons-32/007-tool-2.png"></div><h6 class="title-font">Políticas</h6></a></li>
              </ul>
            </nav> 
          </div> <!-- .cd-slider-nav -->

          <ul class="cd-hero-slider">

            <li class="selected">
              <div class="heading">
                <h1 class="title-font">COMIC PROJECT</h1>
                <span class="text-font">EL mejor lugar para crear y leer comics</span>
              </div>
              <div class="cd-full-width first-slide">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="content first-content">
                        <h4 class="title-font">¿Qué es Comic Project?</h4>
                        <p class="text-font">Comic Project es una plataforma donde puedes compartir comics propios con sus capítulos, descripciónes e imagenes a otros usuarios y amigos. En Comic Project también puedes ser un seguidor de comics habiendo más de 10 géneros diferentes para escojer, siempre encontrarás algo que leer.</p>
                        <div class="talk-bubble tri-right btm-right">
                          <div class="talktext">
                            <a class="title-font" href="login">Registrarse</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </li>

            <li>
              <div class="heading">
                <h1 class="title-font">A cerca de Nosotros</h1>
                <span class="text-font">Información de Comic Project</span> 
              </div>
              <div class="cd-half-width second-slide">   
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="content second-content">
                        <div class="row">
                          <div class="col-md-7 left-image">
                            <img src="img/image-pilot/left-about-image.jpg">
                          </div>
                          <div class="col-md-5">
                            <div class="right-about-text">
                              <h4 class="title-font">¿Quiénes somos?</h4>
                              <p class="text-font">Organización sin fines de lucro, con motivos educativos, desarrollando plataformas intuitivas y responsivas de temas de interes</p>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-7">
                            <div class="left-about-text">
                              <h4 class="title-font">¿Qué hacemos?</h4>
                              <p class="text-font">Este projecto, como muchos otros, tienen un critério completamente educativo. Su finalidad es la creación de mejores paginas con diseños responsivos y completamente funcionales que empleen cada vez más, nuevas funcionalidades con frameworks actuales y estandarisados.</p>
                            </div>
                          </div>
                          <div class="col-md-5 right-image">
                            <img src="img/image-pilot/right-about-image.jpg">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </li>

            <li>
              <div class="heading">
                <h1 class="title-font">Comics</h1>
                <span class="text-font">Más recomendados</span> 
              </div>
              <div class="cd-half-width fourth-slide">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="content fourth-content">
                        <div class="row">
                          <div class="col-md-3 project-item">
                            <a href="img/image-pilot/item-01.jpg" data-lightbox="image-1"><img src="img/image-pilot/project-item-01.jpg"></a>
                          </div>
                          <div class="col-md-3 project-item">
                            <a href="img/image-pilot/item-02.jpg" data-lightbox="image-1"><img src="img/image-pilot/project-item-02.jpg"></a>
                          </div>
                          <div class="col-md-3 project-item">
                            <a href="img/image-pilot/item-03.jpg" data-lightbox="image-1"><img src="img/image-pilot/project-item-03.jpg"></a>
                          </div>
                          <div class="col-md-3 project-item">
                            <a href="img/image-pilot/item-04.jpg" data-lightbox="image-1"><img src="img/image-pilot/project-item-04.jpg"></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </li>
            <li>
              <div class="heading">
                <h1 class="title-font">Contactanos</h1>
                <span class="text-font">Estamos en contacto con todos</span> 
              </div>
              <div class="cd-half-width fivth-slide">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="content fivth-content">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="left-info">
                              <p class="text-font">Envianos tus dudas, comentarios o sujerencias por nuestras redes sociales o en un correo electrónico.
                              </p>
                              <ul class="social-icons">
                                <i><a href="#"><i class="fa fa-facebook"></i></a></i>
                                <i><a href="#"><i class="fa fa-twitter"></i></a></i>
                                <i><a href="#"><i class="fa fa-linkedin"></i></a></i>
                                <i><a href="#"><i class="fa fa-rss"></i></a></i>
                                <i><a href="#"><i class="fa fa-behance"></i></a></i>
                              </ul>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="row">
                              <form id="contact" action="" method="post">
                                <div class="col-md-6">
                                  <fieldset>
                                    <input name="name" type="text" class="form-control" id="name" placeholder="Nombre" required="">
                                  </fieldset>
                                </div>
                                <div class="col-md-6">
                                  <fieldset>
                                    <input name="email" type="email" class="form-control" id="email" placeholder="Correo" required="">
                                  </fieldset>
                                </div>
                                <div class="col-md-12">
                                  <fieldset>
                                    <textarea name="message" rows="6" class="form-control" id="message" placeholder="Mensaje" required=""></textarea>
                                  </fieldset>
                                </div>
                                <div class="col-md-12">
                                  <fieldset>
                                     <div class="talk-bubble tri-right btm-right">
                                        <div class="talktext">
                                          <a class="title-font" href="login">Enviar</a>
                                        </div>
                                      </div>
                                  </fieldset>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </li>

            <li>
              <div class="heading">
                <h1 class="title-font">Políticas de privacidad</h1>
                <span class="text-font">¿Qué pasa con los comics?</span> 
              </div>
              <div class="cd-half-width third-slide">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="content third-content">
                        <div class="row">
                          <div class="col-md-7 left-image">
                            <img src="img/image-pilot/left-feature-image.jpg">
                          </div>
                          <div class="col-md-5">
                            <div class="right-feature-text">
                              <h4 class="title-font">Políticas de privacidad</h4>
                              <p class="text-font">Se recuerda que esta es una pagina con usos educativos. Se desarollo como um proyecto escolar, por lo cual no hay una forma de protejer los comics con derechos de autor. En su totalidad es un proyecto escolar donde no podemos hacernos responsables de los trabajos expuestos.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </li>
          </ul> <!-- .cd-hero-slider -->
        </section> <!-- .cd-hero -->
        <footer>
          <p class="text-font">Copyright &copy; 2017 Your Company 
                                
        	| Designed by <a href="http://www.templatemo.com" target="_parent"><em>templatemo</em></a><br>
        	Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
        	</p>
        	
        </footer>
    
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>