<!--!DOCTYPE html>
<html>
<head>
	<title>Vue</title>
	<link href="js/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<script src="js/vue.js"></script>	

</head>
<body>

</body>
	<div id="example">
  <button v-on:click="greet">Greet</button>
  <button v-on:click="prueba">Prueba</button>
  <p> @{{ response.name }} </p>
</div>

	<script type="text/javascript">
		var vm = new Vue({
		  el: '#example',
		  data: {
		    name: 'Vue.js',
		    response :[]
		  },
		  // define methods under the `methods` object
		  methods: {
		    greet: function (event) {
		      // `this` inside methods point to the Vue instance
		      alert('Hello ' + this.name + '!')
		      // `event` is the native DOM event
		      alert(event.target.tagName)
		    },
		    prueba : function (){
		    	axios.get('/registro').then(function (response) {
		    		this.response = response
				}).catch(function (error) {

				});
		    }
		  }
		});
	</script>
</html-->
<!doctype html>
<html>
  <head>
    <title>axios - get example</title>
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"/>
    <script src="js/axios.min.js"></script>
  </head>
  <body class="container">
    <h1>axios.get</h1>
    <ul id="people" class="list-unstyled"></ul>

    <script>
      axios.get('/get/server')
        .then(function (response) {
          document.getElementById('people').innerHTML = response.data.map(function (person) {
            return (
              '<li class="row">' +
                '<img src="https://avatars.githubusercontent.com/u/' + person.avatar + '?s=50" class="col-md-1"/>' +
                '<div class="col-md-3">' +
                  '<strong>' + person.name + '</strong>' +
                  '<div>Github: <a href="https://github.com/' + person.github + '" target="_blank">' + person.github + '</a></div>' +
                  '<div>Twitter: <a href="https://twitter.com/' + person.twitter + '" target="_blank">' + person.twitter + '</a></div>' +
                '</div>' +
              '</li><br/>'
            );
          }).join('');
        })
        .catch(function (err) {
          document.getElementById('people').innerHTML = '<li class="text-danger">' + err.message + '</li>';
        });
    </script>
  </body>
</html>