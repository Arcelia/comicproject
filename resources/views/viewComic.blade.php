<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Comic Project</title>

    <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"-->
    
    <!-- Bootstrap core CSS -->
    <link href="js/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="css/reset.css">
    <link href="css/4-col-portfolio.css" rel="stylesheet">
    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link href="css/colors.css" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="css/checkbox.css">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/componentReset.css">
    <link rel="stylesheet" type="text/css" href="css/scrollBar.css">
    <link rel="stylesheet" type="text/css" href="css/carousel.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <style type="text/css">
      /* carousel */
      .media-carousel {
        margin-bottom: 0;
        padding: 0 40px 30px 40px;
        margin-top: 30px;
      }
      /* Previous button  */
      .media-carousel .carousel-control.left {
        left: -12px;
        background-image: none;
        background: none repeat scroll 0 0 #222222;
        border: 4px solid #FFFFFF;
        border-radius: 23px 23px 23px 23px;
        height: 40px;
        width : 40px;
        margin-top: 1000px
      }
      /* Next button  */
      .media-carousel .carousel-control.right {
        right: -12px !important;
        background-image: none;
        background: none repeat scroll 0 0 #222222;
        border: 4px solid #FFFFFF;
        border-radius: 23px 23px 23px 23px;
        height: 40px;
        width : 40px;
        margin-top: 1000px
      }
      /* Changes the position of the indicators */
      .media-carousel .carousel-indicators {
        top: auto;
        bottom: 0px;
      }
      /* Changes the colour of the indicators */
      .media-carousel .carousel-indicators li 
      {
        background: #c0c0c0;
      }
      .media-carousel .carousel-indicators .active 
      {
        background: #333333;
      }
      .media-carousel img
      {
        width: 1000px;
        height: 2000px
      }
      body{
        padding: 0px;
      }
      @media (min-width: 768px)
      .navbar {
          border-radius: 0px;
      }
      .navbar {
          position: relative;
          min-height: 50px;
          margin-bottom: 0px;
          border: 1px solid transparent;
      }

    </style>
  </head>

  <body>
    <!-- Navigation -->
    @include('section.header')

    <!-- Page Content -->
    <div id="wrapper" class="container">
      <div>
        <div id="sidebar-wrapper">
          <ul class="sidebar-nav">
           <li class="sidebar-brand">
              <label class="white-text title-font">Capítulos</label>
            </li>
            <li>
              <div class="check white-text">
                <input id="1" type="checkbox"/>
                <label for="1">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">Capítulo 1</p>
            </li>
            <li>
              <div class="check white-text">
                <input id="2" type="checkbox"/>
                <label for="2">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">Capítulo 2</p>
            </li>
            <li>
              <div class="check white-text">
                <input id="3" type="checkbox"/>
                <label for="3">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">Capítulo 3</p>
            </li>
            <li>
              <div class="check white-text">
                <input id="4" type="checkbox"/>
                <label for="4">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">Capítulo 4</p>
            </li>
            <li>
              <div class="check white-text">
                <input id="5" type="checkbox"/>
                <label for="5">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">Capítulo 5</p>
            </li>
          </ul>
        </div>
      </div>
      <a href="#menu-toggle" class="btn btn-secondary text-font" id="menu-toggle">Categorías</a>
      <!-- Page Heading -->
     <div class="container">
      <div class="row">
        <h3>Titulo</h3>
      </div>
      <div class='row' id="viewComic">
        <div class='col-md-12'>
          <div class="carousel slide media-carousel" id="media">
            <div class="carousel-inner">
              <carousel ></carousel>
            </div>
            <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
            <a data-slide="next" href="#media" class="right carousel-control">›</a>
          </div>                          
        </div>
      </div>
    </div>
          <!-- /.container -->
          <!-- Footer -->
   
    <script src="js/vendor/jquery/jquery.min.js"></script>
    <script>
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
    </script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/vendor/popper/popper.min.js"></script>
    <script src="js/vendor/bootstrap/js/bootstrap.js"></script>
  </body>
</html>