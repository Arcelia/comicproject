<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Comic List</title>

    <!-- Bootstrap core CSS -->
    <link href="js/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="css/reset.css">
    <!-- Custom styles for this template -->
    <link href="css/1-col-portfolio.css" rel="stylesheet">
    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link href="css/colors.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/checkbox.css">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/componentReset.css">
    <link rel="stylesheet" type="text/css" href="css/scrollBar.css">
    <script type="text/javascript" src="js/vue.js"></script>
  </head>
  <body>

    <!-- Navigation -->
    @include('section.header')
     <!-- Page Content -->
    <div id="wrapper" class="container">
      <div>
        <div id="sidebar-wrapper">
          <ul class="sidebar-nav">
            <li class="sidebar-brand">
              <label class="white-text title-font">Géneros</label>
            </li>
            @foreach($genders as $gender)
            <li>
              <div class="check white-text">
                <input id="{{ 'gender'.$gender->id }}" type="checkbox"/>
                <label for="{{ 'gender'.$gender->id }}">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">{{ $gender->name }}</p>
            </li>
            @endforeach
            <li class="sidebar-brand">
              <label class="white-text reset-p title-font">Clasificaciones</label>
            </li>
            @foreach($classifications as $classification)
            <li>
              <div class="check white-text">
                <input id="{{ 'classification'.$classification->id }}" type="checkbox"/>
                <label for="{{ 'classification'.$classification->id }}">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">{{ $classification->name }}</p>
            </li>
            @endforeach
            <li class="sidebar-brand">
              <label class="white-text title-font">Tipos</label>
            </li>
            @foreach($types as $type)
            <li>
              <div class="check white-text">
                <input id="{{ 'type'.$type->id }}" type="checkbox"/>
                <label for="{{ 'type'.$type->id }}">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">{{ $type->name }}</p>
            </li>
            @endforeach
            <li class="sidebar-brand">
              <label class="white-text title-font">Historias</label>
            </li>
            @foreach($statusComics as $statusComic)
            <li>
              <div class="check white-text">
                <input id="{{ 'statusComic'.$statusComic->id }}" type="checkbox"/>
                <label for="{{ 'statusComic'.$statusComic->id }}">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">{{ $statusComic->name }}</p>
            </li>
            @endforeach
            <li class="sidebar-brand">
              <label class="white-text title-font">Mis listas</label>
            </li>
            @foreach($statusLists as $statusList)
            <li>
              <div class="check white-text">
                <input id="{{ 'statusList'.$statusList->id }}" type="checkbox"/>
                <label for="{{ 'statusList'.$statusList->id }}">
                  <div class="box"><i class="fa fa-check"></i></div>
                </label>
              </div>
              <p class="white-text reset-p text-font">{{ $statusList->name }}</p>
            </li>
            @endforeach
          </ul>
        </div>
      </div>
      <a href="#menu-toggle" class="btn btn-secondary text-font" id="menu-toggle">Categorías</a>
    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading -->
      <h1 class="my-4 title-font">Page Heading
        <small></small>
      </h1>

      <div id="comicList">
        <!-- Project One -->
        @foreach($comicsSearch as $comicSearch)
          <card 
            idcomic='{{ $comicSearch->id }}' 
            title='{{ $comicSearch->comicName }}'
            description ='{{ $comicSearch->description }}' 
            pathcover='{{ $comicSearch->cover }}'
            user_id=' {{ $comicSearch->user_id }}'
            type='{{ $comicSearch->type_id }}'
            typename='{{ $comicSearch->typeName }}'
            statuscomic='{{ $comicSearch->statusComic_id }}'
            statuscomicname='{{ $comicSearch->statusComicsName }}'
            classifications='{{ $comicSearch->classification_id }}'
            classificationsname='{{ $comicSearch->classificationsName }}'
            >
          </card>
        @endforeach
        <!-- /.row -->
        <hr>
      </div>

      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only class="text-font"">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link class="text-font"" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link class="text-font"" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link class="text-font"" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only text-font">Next</span>
          </a>
        </li>
      </ul>

    </div>
    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
      </div>
      <!-- /.container -->
    </footer>
<script src="js/vendor/jquery/jquery.min.js"></script>
    <script>
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
    </script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/vendor/popper/popper.min.js"></script>
    <script src="js/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/vue/comicListVue.js"></script>
</html>