<!DOCTYPE html>
<html>
<head>
	@include('section.head')
	<title>Comic Project</title>

	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="css/header.css">
	<!-- Custom jQuery -->
	<script src="js/jquery/header.js" type="text/javascript"></script>
</head>
  <nav class="navbar navbar-fixed-top">
	@include('section.header')	
  <div class="container-fluid principal">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button> 
      <p class="navbar-brand title-font reset-label">Comic Project</p> 
    </div>
    <div class="collapse navbar-collapse principal" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active title-font"><a href="#comicProject" class="reset-a">Inicio</a></li>
        <li><a href="#aboutUs" class="title-font reset-a">About us</a></li>
        <li><a href="#thisProject" class="title-font reset-a">This Project</a></li>
        <li><a href="#contactUs" class="title-font reset-a">Contact us</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>


	<!-- Comic Project -->
	<div id="comicProject" class="reset-div col-md-12 white">
		<img class="col-md-12 reset-img principal-img" src="img/background.png">
		<div class="white max-login">
			<h2 class="title-font">Login</h2>
			<form>
				<input type="email" name="email"><br>
				<input type="password" name="pass"><br>
				<input type="submit" name="login" value="Login">
			</form>
		</div>
	</div>

	<!-- About Us -->
	<div id="aboutUs" class="reset-div col-md-12 white">
		<img class="col-md-12 reset-img principal-img" src="img/background.png">
		About us
	</div>

	<!-- This Project -->
	<div id="thisProject" class="reset-div col-md-12 white">
		<img class="col-md-12 reset-img principal-img" src="img/background.png">
		This project
	</div>

	<!-- Contact Us -->
	<div id="contactUs" class="reset-div col-md-12 white">
		<img class="col-md-12 reset-img principal-img" src="img/background.png">
		Contact us
	</div>

</body>
</html>