<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Comic Project</title>
    <!-- Bootstrap core CSS -->
    <link href="js/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="css/reset.css">
    <link href="css/4-col-portfolio.css" rel="stylesheet">
    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link href="css/colors.css" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="css/profile.css">

    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/componentReset.css">
    <link rel="stylesheet" type="text/css" href="css/scrollBar.css">
  </head>

  <body>
    <!-- Navigation -->
    @include('section.header')
    <!-- Page Content -->
    <div id="wrapper" class="container">
      <div>
        <div id="sidebar-wrapper">
          <div class="card hovercard">
            <div class="cardheader"></div>
            <div class="avatar">
                <img alt="" src="img/image-pilot/profile.jpg">
            </div>
            <div class="info">
                <div class="title text-left title-font">
                    <a target="_blank" href="http://scripteden.com/">Nombre</a>
                    <img style="cursor: pointer; width: 20px" src="img/icons/icons-32/003-write-white.png">
                </div>
                <div class="desc text-left text-font">Acerca de mi</div>
                <div class="desc text-left text-font"></div>
                <div class="desc text-left text-font"></div>
                <div class="desc text-left text-font">Agregar Comic<img src="img/icons/icons-32/002-square.png"></div>
            </div>
            <div class="bottom"></div>
          </div>
        </div>
      </div>
      <a class="btn"></a>
      <!-- Page Heading -->
      <h1 class="my-4 title-font">Leyendo
        <small></small>
      </h1>
<div class="row">
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project One</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project One</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
      </div>
      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only text-font">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only text-font">Next</span>
          </a>
        </li>
      </ul>
      <!-- /.row -->
      <h1 class="my-4 title-font">Mis historias
        <small></small>
      </h1>
      <div class="row">
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/icons-pilot/add.png" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Nueva Historia</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project One</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
      </div>
      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only text-font">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only text-font">Next</span>
          </a>
        </li>
      </ul>
      <h1 class="my-4 title-font">Leídos
        <small></small>
      </h1>
      <div class="row">
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project One</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project One</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
      </div>
      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only text-font">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only text-font">Next</span>
          </a>
        </li>
      </ul>
        <h1 class="my-4 title-font">Por leer
          <small></small>
        </h1>
        <div class="row">
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project One</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project One</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Two</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/image-pilot/cover.jpg" alt=""></a>
            <div class="card-body">
              <h6 class="card-title text-center">
                <a class="text-font" href="#">Project Three</a>
              </h6>
            </div>
          </div>
        </div>
      </div>
      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only text-font">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link text-font" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only text-font">Next</span>
          </a>
        </li>
      </ul>
      </div>
    </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white text-font">Snippet by codename065</p>
      </div>
      <!-- /.container -->
    </footer>
    <script src="js/vendor/jquery/jquery.min.js"></script>
    <script>
     
        $("#wrapper").toggleClass("toggled");
    </script>

    <script>
        $("#addComic").toggleClass("toggled");
    </script>

    <!-- Bootstrap core JavaScript -->
    <script src="js/vendor/popper/popper.min.js"></script>
    <script src="js/vendor/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>