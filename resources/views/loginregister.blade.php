<!DOCTYPE html>
<html>
<head>
  @include('section.head')
  <link rel="stylesheet" type="text/css" href="css/login.css">
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

  <link rel="stylesheet" type="text/css" href="css/fonts.css">
  <link rel="stylesheet" href="css/templatemo-style.css">
  <link rel="stylesheet" href="css/checkbox.css">
  <link rel="stylesheet" href="css/componentReset.css">
  <script src="js/jquery/login.js" type="text/javascript"></script>
  <title>Registro</title>
</head>
<body>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="">
          <a href="#" class="selected" id="login-form-link">
            <div class="image-icon">
              <img src="img/icons/icons-32/001-user-hand-drawn-outline-in-a-rectangle.png">
            </div>
              <h6 class="title-font">Login</h6>
          </a>
        </li>
        <li>
          <a href="#" id="register-form-link" class="">
            <div class="image-icon">
              <img src="img/icons/icons-32/003-write.png">
            </div>
            <h6 class="title-font">Registro</h6>
          </a>
        </li>
      </ul>
    </nav> 
  </div> <!-- .cd-slider-nav -->
  <div class="container">
     <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-login">
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">
                <form id="login-form" action="#" method="post" role="form" style="display: block;">
                  <h2 class="title-font">LOGIN</h2>
                    <div class="form-group">
                      <input type="email" name="username" id="username" tabindex="1" class="form-control text-font" placeholder="Correo" value="" required>
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" id="password" tabindex="2" class="form-control text-font" placeholder="Contraseña" required>
                    </div>
                    <div class="col-xs-6 form-group pull-left checkbox">
                      <div class="check white-text">
                        <input id="check" type="checkbox"/>
                        <label for="check" hidden id="checkLogin">
                          <div class="box"><i class="fa fa-check"></i></div>
                        </label>
                      </div>
                      <p class="reset-p text-font">Recordar cuenta</p>   
                    </div>
                    <div class="pull-right" style="margin: 0px 30px;">
                      <div class="talk-bubble tri-right btm-right">
                        <div class="talktext">
                          <button class="title-font btn-hidden">
                            Registrarse
                          </button>
                        </div>
                      </div>
                    </div>
                </form>
                <form id="register-form" action="#" method="post" role="form" style="display: none;">
                  <h2 class="title-font">REGISTRO</h2>
                    <div class="form-group">
                      <input type="text" name="username" id="username" tabindex="1" class="form-control text-font" placeholder="Nombre" value="" required>
                    </div>
                    <div class="form-group">
                      <input type="email" name="email" id="email" tabindex="1" class="form-control text-font" placeholder="Correo electrónico" value="" required>
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" id="password" tabindex="2" class="form-control text-font" placeholder="Contraseña" required>
                    </div>

                    <div class="form-group"> 
                      <label class="control-label text-font text-size16" for="username">Fecha de nacimiento</label>
                      <div class="controls">
                      <input type="date" name="confirm-password" id="confirm-password" tabindex="2" class="form-control text-font text-size16" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="happy" class="control-label text-left text-font text-size16">Género</label>
                        <div class="input-group">
                          <div id="radioBtn" class="btn-group">
                            <a class="btn btn-sm active text-font principal-white text-size16" data-toggle="happy" data-title="M">Hombre</a>
                            <a class="btn principal-white btn-sm notActive text-font text-size16" data-toggle="happy" data-title="F">Mujer</a>
                          </div>
                      </div>
                    </div>

                    <div class="form-group pull-right" style="margin: 0px 15px;">
                      <div class="row text-center">
                        <div class="talk-bubble tri-right btm-right">
                          <div class="talktext">
                           <button class="title-font btn-hidden">
                            Crear cuenta
                          </button>
                          </div>
                        </div>
                      </div>
                    </div>
                </form>
              </div>
            </div>
          </div>
          <!--div class="panel-heading">
            <div class="row">
              <div class="col-xs-6 tabs">
                <a href="#" class="active" id="login-form-link"><div class="login title-font">LOGIN</div></a>
              </div>
              <div class="col-xs-6 tabs">
                <a href="#" id="register-form-link"><div class="register title-font">REGISTRO</div></a>
              </div>
            </div>
          </div-->
        </div>
      </div>
    </div>
  </div>
  <div class="container">
      <div class="col-md-10 col-md-offset-1 text-center">
          <h6 style="font-size:14px;font-weight:100;color: #fff;">Coded with <i class="fa fa-heart red" style="color: #BC0213;"></i> by <a href="http://hashif.com" style="color: #fff;" target="_blank">Hashif</a></h6>
      </div>   
  </div>
  </body>
</html>