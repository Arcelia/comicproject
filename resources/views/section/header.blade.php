<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand title-font" href="#">Comic Project</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" 
        data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <form class="navbar-form navbar-left" role="search">
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <input type="text" class="form-control" placeholder="Search">
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item ">
              <a class="nav-link text-font" href="/profile">Perfil</a>
              <span class="sr-only">(current)</span>
            </li>
            <li class="nav-item">
              <a class="nav-link active text-font" href="/ComicProject">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-font" href="/login">Salir</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>