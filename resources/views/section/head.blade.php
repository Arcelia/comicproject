@section('head')
    <!-- Configuration -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Comic Proyect">
    <meta name="author" content="">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap-3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS Default -->
    <link href="css/reset.css"  rel="stylesheet" type="text/css">
    <link href="css/colors.css" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link rel="stylesheet" type="text/css" href="css/fonts.css">

    <!-- jQuery -->
    <script src="js/bootstrap/jquery-3.2.1.min.js" type="text/javascript"></script>

    <!-- Bootstrap Core JS -->
    <script src="css/bootstrap-3.3.7/dist/js/bootstrap.min.js" type="text/javascript"></script>
@show