<?php

/*
 * Rutas
 * @author Arcelia Aguirre
*/


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Welcome Laravel
 * @return welcome 
 * @status completo
*/
Route::get('/welcome', function () {
    return view('welcome');
});

/*
 * Landing Page
 * @return index
 * @status completo
*/
Route::get('/', function () {	
    return view('index');
});

/*
 * Login y registro
 * Validacion en controller
 * @return loginregister 
 * @status incompleto
*/
Route::get
('/login', function () {
	// $user = new App\Model\User;
	// $user->	
	//App::make('\App\Http\Controllers\UsersController')->store();
    return view('loginregister');
});

/*
 * Perfil
 * Mandar el perfil por id
 * @return profile 
 * @status incompleto
*/
Route::get('/profile', function () {
    return view('profile');
});

/*
 * Dashboard
 * Cargar toda la informacion
 * @return dashboard 
 * @status incompleto
*/
Route::get('/ComicProject', function () {
	//$genders = DB::table('genders')->get();
	$genders = App::make('\App\Http\Controllers\GendersController')->index();
	$classifications = App::make('\App\Http\Controllers\ClassificationsController')->index();
	$types = App::make('\App\Http\Controllers\TypesController')->index();
	$statusComics = App::make('\App\Http\Controllers\StatusComicsController')->index();
	$statusLists = App::make('\App\Http\Controllers\StatusListsController')->index();
	$comicsNew = App::make('\App\Http\Controllers\ComicsController')->index('L1');
	$comicsView = App::make('\App\Http\Controllers\ComicsController')->index('L2');

	return View::make('dashboard')
		->with('genders', $genders)
		->with('classifications', $classifications)
		->with('types', $types)
		->with('statusComics', $statusComics)
		->with('statusLists', $statusLists)
		->with('comicsNew', $comicsNew)
		->with('comicsView', $comicsView);
});

/*
 * Lista de comics
 * Mandar id de usuario, mandar el id 
 * @return loginregister 
 * @status incompleto
*/
Route::get('/comicList', function () {
	$genders = App::make('\App\Http\Controllers\GendersController')->index();
	$classifications = App::make('\App\Http\Controllers\ClassificationsController')->index();
	$types = App::make('\App\Http\Controllers\TypesController')->index();
	$statusComics = App::make('\App\Http\Controllers\StatusComicsController')->index();
	$statusLists = App::make('\App\Http\Controllers\StatusListsController')->index();
	$comicsSearch = App::make('\App\Http\Controllers\ComicsController')->index('L3');

	return View::make('comicList')
		->with('genders', $genders)
		->with('classifications', $classifications)
		->with('types', $types)
		->with('statusComics', $statusComics)
		->with('statusLists', $statusLists)
		->with('comicsSearch', $comicsSearch);
});

/*
 * Lectura un comic
 * Mandar id de comic
 * @return loginregister 
 * @status incompleto
*/
Route::get('/comic/{id}', function ($id) {
	$comicsView = App::make('\App\Http\Controllers\ComicsController')->index('L2');
    return view('viewComic');
});

/*
 * Lectura un comic
 * Mandar id de comic
 * @return loginregister 
 * @status incompleto
*/
/*Route::get('/chapterCreate', function () {
    return view('chapterList');
});*/



/* ------------ Login ------------ */
// Authentication routes...
/*Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


/*Route::controllers([
   'password' => 'Auth\PasswordController',
]);
*/


/* ------------ Pruebas ------------ */

/*
 * Pruebas de controladores standar
 * @return standar
*/
Route::get('/standar', function () {
    return view('pages.standar');
});


/*
 * Pruebas de imagenes
 * @return pruebasImagen 
 * @status incompleto
*/
Route::get('/pruebas', function () {
    return view('pruebasImagen');
});

/*
 * Pruebas de popup
 * @return pruebasImagen 
 * @status incompleto
*/
Route::get('/popup', function () {
    return view('popup');
});

/*
 * Pruebas de fuentes
 * @return landingpage
 * @status incompleto
*/
Route::get('/font', function () {
    return view('landingPage');
});


/*
 * Pruebas de vue
 * @return landingpage
 * @status incompleto
*/
Route::get('/vue', function () {
	$genders = App::make('\App\Http\Controllers\GendersController')->index();
    return view('pruebasVue');
});

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');